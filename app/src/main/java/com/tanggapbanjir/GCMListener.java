package com.tanggapbanjir;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

public class GCMListener extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.v("_TES","_TES");
        sendNotif(data.getString("message"));
    }
    private void sendNotif(String message) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("status", message);
        PendingIntent pi = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_ONE_SHOT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notif = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logogundar)
                .setContentTitle("Tanggap Banjir")
                .setContentText("Tinggi air saat ini " + message + " cm")
                .setAutoCancel(true)
                .setSound(sound)
                .setContentIntent(pi);
        NotificationManager notifManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.notify(1, notif.build());
    }
}
