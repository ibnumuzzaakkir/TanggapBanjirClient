package com.tanggapbanjir;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class lihat_status extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters
    private String statusAir;


    public lihat_status() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment lihat_status.
     */
    // TODO: Rename and change types and number of parameters
    public static lihat_status newInstance(String param1) {
        lihat_status fragment = new lihat_status();
        Bundle args = new Bundle();
        args.putString("newStatus", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            statusAir = getArguments().getString("newStatus");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lihat_status, container, false);
        ImageView imageView = (ImageView)view.findViewById(R.id.imageStatus);
        SharedPreferences prefs = getActivity().getSharedPreferences("data_air", Context.MODE_PRIVATE);
        String restoredText = prefs.getString("tinggiAir", null);
        Log.d("TES","STATUS AIR"+ restoredText);

            int statusAirBaru = Integer.parseInt(restoredText);
            if(statusAirBaru< 750){
                imageView.setImageResource(R.drawable.hijau);
            }else if(statusAirBaru >= 750 && statusAirBaru <= 849){
                imageView.setImageResource(R.drawable.biru);
            }else if(statusAirBaru >= 850 && statusAirBaru <= 949){
                imageView.setImageResource(R.drawable.kuning);
            }else if(statusAirBaru > 950){
                imageView.setImageResource(R.drawable.merah);
            }

        return view ;
    }

}
