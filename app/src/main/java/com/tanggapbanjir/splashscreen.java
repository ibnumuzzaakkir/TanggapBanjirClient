package com.tanggapbanjir;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class splashscreen extends Activity {
    private static final String TAG = "SplashScreen";

    BroadcastReceiver registrasi;
    private static final int PLAY_SERVICES = 9000;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        registrasi = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                    Thread splashThread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                sleep(2000);
                            }
                            catch(InterruptedException e) {
                                Log.d("DEBUG_"+TAG, "error?");
                            }
                            finally {
                                Intent i = new Intent(splashscreen.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }
                    };
                    splashThread.start();
            }
        };
        if (cekPlayService()) {
            Intent i = new Intent (splashscreen.this, Registrasi.class);
            startService(i);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(registrasi, new IntentFilter("Selesai"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(registrasi);
    }

    protected boolean cekPlayService() {
        int kodehasil = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (kodehasil != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(kodehasil)) {
                GooglePlayServicesUtil.getErrorDialog(kodehasil, this, PLAY_SERVICES).show();
            }
            else {
                finish();
            }
            return false;
        }
        return true;
    }
}
